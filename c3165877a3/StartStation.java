
public class StartStation extends Station
{
    public StartStation()
    {
        super();
        unStarve();     //Never starved
    }
    @Override
    //Staring stations can never be starved, just starts a new Item
    public Event startProcessing()
    {
        currentItem = new Item();
        startBusy();
        return new Event(getProcessingTime(), this);
    }
}
