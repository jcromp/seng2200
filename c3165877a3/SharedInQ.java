import java.util.*;
//Queue with two station before both feeding into the station and one after.
public class SharedInQ extends StorageQueue
{
    private Station after;
    private Station beforeL;
    private Station beforeR;

    public SharedInQ()
    {
        super();
        after = null;
        beforeL = null;
        beforeR = null;
    }
    public SharedInQ(int cap, Station bfl, Station bfr, Station af)
    {
        super(cap);
        after = af;
        beforeL = bfl;
        beforeR = bfr;
    }
    @Override
    public void enque(Item it)
    {
        if(!isFull())
        {   storage.add(it);
            items++;
            it.startWaiting();
            if(after.isStarved())
            {
                after.unStarve();
            }
         }        
    }
    @Override
    public Item deque()
    {
        if(!isEmpty())
        {
            Item head = storage.remove();
            items--;
            head.finishWaiting();
            if(beforeL.isBlocked())
            {
                beforeL.unBlock();
            }
            if(beforeR.isBlocked())
            {
                beforeR.unBlock();
            }
            return head;
        }
        return null;
        
    }
}
