import java.util.*;
//Queue with two stations after taking from it and a single before.
public class SharedOutQ extends StorageQueue
{
    private Station before;
    private Station afterL;
    private Station afterR;
    public SharedOutQ()
    {
        super();
        before = null;
        afterL = null;
        afterR = null;
    }
    public SharedOutQ(int cap, Station bf, Station afl, Station afr)
    {
        super(cap);
        before = bf;
        afterL = afl;
        afterR = afr;
    }
    @Override
    public void enque(Item it)
    {
        if(!before.isBlocked())
        {
            storage.add(it);
            items++;
            it.startWaiting();
            if(afterL.isStarved())
            {
                afterL.unStarve();
            }
            if(afterR.isStarved())
            {
                afterR.unStarve();
            }
        }
    }
    @Override
    public Item deque()
    {
        if(!isEmpty())
        {
            Item head = storage.remove();
            items--;
            head.finishWaiting();
            //Removing item unblocks station before
            if(before.isBlocked())
            {
             before.unBlock();   
            }
            return head;
        }
        return null;
    }
}
