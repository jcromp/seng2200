
public class EndStation extends Station
{
   
    public EndStation()
    {
      super();
      unBlock();      
    }
    @Override
    //End station can never be blocked. Always return true.
    public boolean finishProcessing()
    {
        //OUTPUT ITEM STATISTICS.
        //System.out.println("Finished an Item");
        currentItem.itemFinished();
        currentItem = null;
        //System.out.println(Item.getItemsProduced());
        endBusy();        
        return true;
    }
}
