public class Event implements Comparable<Event>
{
    private double processTime;
    private double finishTime;
    private Station station;

    public Event()
    {
        processTime = 0;
        station = null;
    }
    public Event(double time, Station stat)
    {
        processTime = time;     //Time that the process takes to finish.
        station = stat;         //Station the event is for.
        setFinishTime();
    }
    public void setFinishTime()
    {
        finishTime = PA3.clock() + processTime;
    }
    public Station getStation()
    {
        return station;
    }
    public double getFinishTime()
    {
        return finishTime;
    }
    public double getProcessTime()
    {
        return processTime;
    }
    //Return -1 if arg is bigger than this
    //Return 0 if Events are the same
    //Return 1 if this is bigger than arg
    @Override
    public int compareTo(Event arg)
    {
        if(this.finishTime < arg.getFinishTime())
        {
            return -1;
        }
        else if (this.finishTime == arg.getFinishTime())
        {
            return 0;
        }
        else
        {
            return 1;
        }
        
    }
  
}
