import java.util.*;
public class Station
{
    protected static double MEAN;
    protected static double RANGE;
    protected StorageQueue before;
    protected StorageQueue after;
    protected boolean blocked, starved, busy;
    protected Item currentItem;    
    protected Random r = new Random(System.currentTimeMillis());    
    protected double startStarving, timeStarved = 0;
    protected double startBlocking, timeBlocked = 0;
    protected double startBusy, timeBusy = 0;
    public Station()
    {
        before = null;
        after = null;
        unBlock();
        starve();
        endBusy();
    }
    public Station(StorageQueue bf, StorageQueue af)
    {
        before = bf;
        after = af;
        unBlock();
        starve();
        endBusy();
    }
    public static void setMean(double m)
    {
        MEAN = m;
    }
    public static void setRange(double r)
    {
        RANGE = r;
    }
    //When a station tries to finish an item but Q after is full. It must block.
    public void block()
    {
        startBlocking =PA3.clock();
        blocked = true;
    }
    //When unblocking record how long the station spent blocked
    public void unBlock()
    {
        timeBlocked += PA3.clock() - startBlocking;
        blocked = false;
    }
    public void starve()
    {
        startStarving = PA3.clock();
        starved = true;
    }
    //When unstarving record how long the station spent blocked
    public void unStarve()
    {
        timeStarved += PA3.clock() - startStarving;
        starved = false;
    }
    public void startBusy()
    {
        startBusy = PA3.clock();
        busy = true;
    }
    public void endBusy()
    {
        timeBusy += PA3.clock() - startBusy;
        busy = false;
    }
    public boolean isBlocked()
    {
        return blocked;
    }
    public boolean isStarved()
    {
        return starved;
    }
    public boolean isBusy()
    {
        return busy;
    }
    public void setQueueAfter(StorageQueue af)
    {
        after = af;
    }
    public void setQueueBefore(StorageQueue bf)
    {
        before = bf;
    }
    //Return the amount of time spent starved as a %
    public double getTimeStarved()
    {
        return (timeStarved/PA3.getTimeLimit())*100;
    }
    //Return the amount of time spent blocked as a %
    public double getTimeBlocked()
    {
        return (timeBlocked/PA3.getTimeLimit())*100;
    }
    //Return the amount of time spent producing as a %
    public double getProductionTime()
    {
        return (timeBusy/PA3.getTimeLimit())*100;
    }
    //Creates and returns an event of starting the next item
    //If q befor is empty must starve.
    public Event startProcessing()
    {
        if(!before.isEmpty())
        {
            currentItem = before.deque();
            startBusy();
            return new Event(getProcessingTime(), this);
        }
        else 
        {
            if(!isStarved())
            {
                starve();
            }
            return null;
        }
    }
    //returns random time according to range and mean.
    public double getProcessingTime()
    {
        double processTime;
        double d = r.nextDouble();
        processTime = MEAN + RANGE*(d-0.5);
        return processTime;
    }
    //Moves the item into storage and returns true
    //or Holds item and returns false if it is blocked
    public boolean finishProcessing()
    {
        endBusy();
        if(!after.isFull())
        {
            after.enque(currentItem);
            currentItem = null;
            return true;                     
        }
        else
        {
            block();
            return false;   
        }    
    }
    //When a blocked event can finish with an Item
    public boolean finishWithItem()
    {
        if(!after.isFull())
        {
            after.enque(currentItem);
            currentItem = null;
            return true;
        }
        else
        {
            unBlock();      //must unblock then re block in order to maintain consistent statistics
            block();       
            return false;
        }
    }
}

