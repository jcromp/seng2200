import java.util.*;
//Queue with one station before and one after.
public class StraightQ extends StorageQueue
{
    private Station after;
    private Station before;
    public StraightQ()
    {
        super();
        after = null;
        before = null;
    }
    public StraightQ(int cap, Station bf, Station af)
    {
        super(cap);
        before = bf;
        after = af;
    }
    @Override
    public void enque(Item it)
    {
        if(!before.isBlocked())
        {
            storage.add(it);
            items++;
            it.startWaiting();
            if(after.isStarved())
            {
                after.unStarve();
            }
        }        
    }
    @Override
    public Item deque()
    {
        if(!isEmpty())
        {
            Item head = storage.remove();
            items--;
            head.finishWaiting();
            //If was full (stations before blocked) they can now finish their item
            if(before.isBlocked())
            {
                before.unBlock();
            }
            return head;
        }
        return null;
    }
    
}

