
public class Item
{
    private double startQTime, waitTime; 
    //private static double waitTimeAllItems=0;
    private static int itemsProduced=0;
    private int qCounter = 0;
    private double[] qWaitTimes;
    public Item()
    {
        waitTime = 0;
        startQTime = 0;
        qWaitTimes = new double[PA3.getNumOfQueues()];
    }
    //Record what time the Item enters the queue
    public void startWaiting()
    {
        startQTime = PA3.clock();
    }
    //Finds difference between when started in q and current (leaving) time and adds to waittime for that q
    public void finishWaiting()
    {
        waitTime = (PA3.clock() - startQTime);
        qWaitTimes[qCounter] = waitTime;
        qCounter++;
    }
    //Item is finished at final station.
    public void itemFinished()  //Item it
    {
        itemsProduced++;
        StorageQueue.addWaitTimes(qWaitTimes);
    }
    public static int getItemsProduced()
    {
        return itemsProduced;
    }
}
