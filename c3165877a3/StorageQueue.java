import java.util.*;
public class StorageQueue
{
    protected Queue<Item> storage;
    protected int capacity;
    protected int items;
    private double averageLength;
    protected static double[] totalwaitTimes;
    public StorageQueue()
    {
        storage = null;
        capacity = 0;
        items = 0;
    }
    public StorageQueue(int cap)
    {
        capacity = cap;
        items = 0;
        averageLength = 0;
        storage = new ArrayDeque<Item>(cap);
    }
    //Sets the length of array according to the number of queues in the factory.
    public static void setQueues(int qs)
    {
        totalwaitTimes = new double[qs];
    }
    public boolean isFull()
    {
       return items==capacity;
    }
    public boolean isEmpty()
    {
        return items==0;
    }
    //Add item to queue
    public void enque(Item it)
    {
         if(!isFull())
        {
            storage.add(it);
            items++;
            it.startWaiting();
        }            
    }
    //remove Item from queue
    public Item deque()
    {
        if(!isEmpty())
        {
            items--;
            Item head = storage.remove();
            head.finishWaiting();
            return head;
        }
        return null;
    }
    //Adds up the total wait times of all items in each queue. Time spent waiting in each queue.
    public static void addWaitTimes(double[] times)
    {
        for(int i= 0; i < times.length; i++)
        {
            totalwaitTimes[i] += times[i];
        }
    }
    //returns the average wait time for an Item in the specified queue.
    public double getWaitTime(int index)
    {
        return totalwaitTimes[index]/Item.getItemsProduced();
    }
    //Queue did not change lngth in the time that has pased
    //therefore the average length will be the length at each time interval/total time units
    //add up the length of the queue at each time interval (items*timePassed) then divide by TIMELIMIT at the end
    //result will be the average q length throughout the program runtime
    
    public void updateAverageLength(double timePassed)
    {
        averageLength += items*timePassed;
    }
    public double getAverageQLength()
    {        
        return averageLength/PA3.getTimeLimit();
    }
}
