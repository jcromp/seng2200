import java.util.*;
import java.text.DecimalFormat;
import java.util.PriorityQueue;
public class PA3
{
    private static final int TIMELIMIT = 10000000;          //Simulation Time
    private static double clock = 0;                        //Current time
    private static int numOfQs = 0;
    private PriorityQueue<Event> events;                    
    private ArrayList<Event> blockedEvents;                 //If events cant run they are added to a list
    public static void main(String[] args)
    {
        PA3 aFactory = new PA3();
        aFactory.start();
    }
    public static int getTimeLimit()
    {
        return TIMELIMIT;
    }
    //Anyone/thing in the factory can see what time it is.
    public static double clock()
    {
        return clock;
    }
    //Once queues are created set the number so statistics can be worked out
    public void setNumOfQs(int qs)
    {
        numOfQs = qs;
        StorageQueue.setQueues(numOfQs);        //Makes array for holding each queues wait time
    }
    public static int getNumOfQueues()
    {
        return numOfQs;
    }
    public void start()
    {        
        Scanner kb = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("##.##");
        final int LENGTH;
        System.out.println("Factory Starting");     
        System.out.println("Enter the Mean:");
        double x = kb.nextDouble();
        Station.setMean(x);
        System.out.println("Enter the Range:");
        x = kb.nextDouble();
        Station.setRange(x);
        System.out.println("Please Enter the Maximum Queue length for the Factory");
        LENGTH = kb.nextInt();
        //Build the factory
        events = new PriorityQueue<Event>(8);
        blockedEvents = new ArrayList<Event>(8);
        StartStation s0 = new StartStation();
        Station s1 = new Station();
        ParallelStation s2a = new ParallelStation();
        ParallelStation s2b = new ParallelStation();
        Station s3 = new Station();
        ParallelStation s4a = new ParallelStation();
        ParallelStation s4b = new ParallelStation();
        EndStation s5 = new EndStation();
        Station[] stations = {s0,s1,s2a,s2b,s3,s4a,s4b,s5};
        StorageQueue q01 = new StraightQ(LENGTH, s0, s1);
        StorageQueue q12 = new SharedOutQ(LENGTH, s1, s2a, s2b);
        StorageQueue q23 = new SharedInQ(LENGTH, s2a, s2b, s3);
        StorageQueue q34 = new SharedOutQ(LENGTH, s3, s4a, s4b);
        StorageQueue q45 = new SharedInQ(LENGTH, s4a, s4b, s5);
        StorageQueue[] queues = {q01, q12, q23, q34, q45};
        s0.setQueueAfter(q01);
        s1.setQueueBefore(q01); s1.setQueueAfter(q12);
        s2a.setQueueBefore(q12); s2a.setQueueAfter(q23);
        s2b.setQueueBefore(q12); s2b.setQueueAfter(q23);
        s3.setQueueBefore(q23); s3.setQueueAfter(q34);
        s4a.setQueueBefore(q34); s4a.setQueueAfter(q45);
        s4b.setQueueBefore(q34); s4b.setQueueAfter(q45);
        s5.setQueueBefore(q45); 
        
        //Set the number of queues in the factory.
        setNumOfQs(queues.length);
        
        //Run factory for dsignated amount of time.
        while(clock <= TIMELIMIT)
        {   
            //Empty Queue of events. Start a new Event at First Station
            if(events.size() == 0)
            {
                events.add(s0.startProcessing());
            }
            //Non empty Queue of events, Process event at front of queue.
            else
            {
                Event nextEvent = events.poll();
                if(nextEvent.getStation().finishProcessing())
                {
                    Event newEvent = nextEvent.getStation().startProcessing();
                    if(newEvent != null)
                    {
                        events.add(newEvent);
                    }
                }
                else
                {
                    blockedEvents.add(nextEvent);       //Add to blocked events list
                }
            }
            //Check and see if any stations became unblocked and finish their item.
            for(Iterator<Event> it = blockedEvents.iterator(); it.hasNext();)
            {
                Event nextEvent = it.next();
                if(nextEvent.getStation().finishWithItem())
                {
                    //If processed remove the evnt from the list
                    it.remove();
                    it = blockedEvents.iterator();       //Must recheck blocked events incase one can now run
                    Event newEvent = nextEvent.getStation().startProcessing();
                    if(newEvent != null)
                    {
                        events.add(newEvent);
                    }
                }
            }
            //Check Each station in line to see if it can begin processing
            for(int i = 0; i < stations.length; i++)
            {
                if(!stations[i].isBusy() && !stations[i].isBlocked())
                {
                    Event newEvent = stations[i].startProcessing();
                    if(newEvent != null)
                    {
                        events.add(newEvent);
                    }
                }
            }
            
            //############################################################################
            //####################Q Length##########################
            //############################################################################
            //State of Factory will be unchanged until Next Event starts. Queue Length will remain the same.
            if(events.peek().getFinishTime() > TIMELIMIT)
            {
                //If factory is about to finish only count up until timelimit
                for(StorageQueue q : queues)
                {
                    q.updateAverageLength(TIMELIMIT - clock);
                }
            }
            else
            {
                //Otherwise count for the entire time until next event
                for(StorageQueue q : queues)
                {
                    q.updateAverageLength(events.peek().getFinishTime()- clock);
                }
            }           
            //After all events finished and new events created at current time.Increment clock.
            clock = events.peek().getFinishTime();            
        }    
        //#################################################################
        //###############Print out Factory Statistics#####################
        //#################################################################
        clock = TIMELIMIT;       //All stations stop working at TIMELIMIT
        System.out.println("#############################################################");
        System.out.println("Factory Statistics");
        System.out.println("#############################################################");
        System.out.println("Items Produced: " + Item.getItemsProduced());
        System.out.println("#############################################################");
        String output = "";
        for(int i = 0; i < stations.length; i++)
        {
            switch(i){
                case 0: output = "Station S0";
                        break;
                case 1: output = "Station S1";
                        break;
                case 2: output = "Station S2a";
                        break;
                case 3: output = "Station S2b";
                        break;
                case 4: output = "Station S3";
                        break;
                case 5: output = "Station S4a";
                        break;
                case 6: output = "Station S4b";
                        break;
                case 7: output = "Station S5";
                        break;                 
            }
            if(stations[i].isBusy())        //All stations stop working, if still producing time still counts
            {
                stations[i].endBusy();
            }
            System.out.println(output);
            System.out.println("Time Spent Starved: " + df.format(stations[i].getTimeStarved()) + "%");
            System.out.println("Time Spent Blocked: " + df.format(stations[i].getTimeBlocked()) + "%");
            System.out.println("Time Spent Producing: " + df.format(stations[i].getProductionTime()) + "%");
            System.out.println("#############################################################");
        }
        System.out.println("#############################################################");
        System.out.println("Average Wait Time in Each Queue");
        System.out.println("#############################################################");
        for(int i = 0; i < queues.length; i++)
        {
            System.out.println("Average wait time in Queue " + (i+1) + ": " + df.format(queues[i].getWaitTime(i)));
        } 
        System.out.println("#############################################################");
        System.out.println("Average Queue Length");
        System.out.println("#############################################################");
        for(int i = 0; i < queues.length; i++)
        {
            System.out.println("Average Queue length of Queue " + (i+1) + ": " + df.format(queues[i].getAverageQLength()));
        }       
      
    }
     
}
